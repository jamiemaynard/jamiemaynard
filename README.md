# Jamie Maynard's README

**Keeper of Tomes and Mysteries at GitLab**.   
[Full Stack Developer](https://handbook.gitlab.com/job-families/engineering/development/fullstack/#fullstack-engineer-handbook) at GitLab

## Introductions

Hi, my name is Jamie and I am a [full stack developer](https://handbook.gitlab.com/job-families/engineering/development/fullstack/#fullstack-engineer-handbook) at [GitLab](http://about.gitlab.com).  I am the *Keeper of Tomes and Mystieres* as I am the [DRI](https://about.gitlab.com/handbook/people-group/directly-responsible-individuals/) and Developer responsible for [GitLab's handbook](https://handbook.gitlab.com) projects. 

## Connect with me

<a href="https://gitlab.com/jamiemaynard"><img src="https://img.shields.io/badge/-GitLab-380D75?&style=plastic&logo=GitLab&logoColor=white" height="25" style="height: 25px" /></a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="https://www.linkedin.com/in/jamiemaynard/"><img src="https://img.shields.io/badge/LinkedIn-0A66C2?&style=plastic&logo=LinkedIn&logoColor=white" height="25" style="height: 25px" /></a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="mailto:jmaynard@gitlab.com"><img src="https://img.shields.io/badge/Email-EA4335?&style=plastic&logo=Gmail&logoColor=white" height="25" style="height: 25px" /></a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="https://calendly.https://calendly.com/jamiemaynard/30min"><img src="https://img.shields.io/badge/Schedule a Meeting-4285F4?&style=plastic&logo=Google Calendar&logoColor=white" height="25" style="height: 25px" /></a>

## Table of Contents

[[_TOC_]]

## About Me

I am a punky goth metal head (The mohawk is real)  Out and about I am mostly found wearing Kilts and Big Boots.  I am a passionate technologist with a passion for Software Development and Open Source.  I am frequently trying to expand my knoweldge and develop my skills which makes being a full stack developer a good fit for me.

## Me at Gitlab

Professionally I am a full stack deevloper (jack of all trades, master of none) charged with looking after the Handbook at GitLab.  I am part of GitLabs [Chief of Staff Team (CoST)](https://handbook.gitlab.com/handbook/ceo/chief-of-staff-team/).  My team is a cross functional team in that we take on inuititives which don't fit in any one single part of the organisation.  This is a good way to look at the Handbook.  It used and contributed to by the whole organisation and as such doesn't belong to any one part of the organisation.

I spend my days generally helping with merge requests (MRs), working on Handbook Issues and tyring to improve the user experience of the Handbook.  My big overriding project at the moment is migrating the Handbooks content from `about.gitlab.com` to `handbook.gitlab.com`.

I try to embrace [GitLabs Values](https://handbook.gitlab.com/values) whenever I can and in everything I do.  I am also pretty passionate about OpenSource and technology in general.

I am fairly approachable and welcome Coffee Chats.

### My current projects and repos

#### Repos

* [New GitLab Handbook](https://handbook.gitlab.com)
    * **Repo:** https://gitlab.com/gitlab-com/content-sites/handbook/
    * **Issues:** https://gitlab.com/gitlab-com/content-sites/handbook/-/issues/
* [About site handbook](https://about.gitlab.com/handbook)
    * **Repo:** https://gitlab.com/gitlab-com/www-gitlab-com
    * **Issues:** Please use the new handbook for issues
* [Internal Handbook](https://internal.gitlab.com)
    * **Repo:** https://gitlab.com/internal-handbook/internal-handbook.gitlab.io
    * **Issues:** 
        * Please use the new public handbook for issues preferably
        * https://gitlab.com/internal-handbook/internal-handbook.gitlab.io/-/issues/
* GitLab Docsy/Hugo Theme
    * **Repo:** https://gitlab.com/gitlab-com/content-sites/docsy-gitlab
    * **Issues:** 
        * Please use the new public handbook for issues preferably
        * https://gitlab.com/gitlab-com/content-sites/docsy-gitlab/-/issues

#### Projects

* **Handbook Migration** - I'm currently migrating all the handbook content from `www-gitlab-com` to the new handbook site.
* **Handbook Support** - Trying to improve handbook support processes to make it easier for team members and the community to contribute to the Handbook
* **Improved Handbook Search** - Making search work better on the handbook so people don't have to resort to Google to try to find the pages they want
* **Handbook Engagement** - I'm starting a program of handbook Engagement activities improve transparence and collaboration on the handbook.  Starting with public issues triage streaming and a monthly public Handbook AMA.  Hopefully starting in October.

For more details generally look at the [Handbook Roadmap](https://handbook.gitlab.com/about/roadmap/)

### My skills

My skillset certainly eclectic but lends itself strongly to to technical.  I'm a versitile software developer who is as comfortable working on Frontend systems as Backend systems.  I have a resonable amount of DevOps skills thrown in to the mix and can work my way round a database if I need to.  I have working understanding of the following languages:

<table>
    <tr>
        <td>Java</td>
        <td>Ruby</td>
        <td>Python</td>
    </tr>
    <tr>
        <td>C</td>
        <td>Go</td>
        <td>SQL</td>
    </tr>
    <tr>
        <td>JavaScript</td>
        <td>TypeScript</td>
        <td>HTML</td>
    </tr>
    <tr>
        <td>CSS</td>
        <td>Terraform</td>
        <td>Bash/ZSH/Shell</td>
    </tr>
    <tr>
        <td>YAML</td>
        <td>JSON</td>
        <td>XML</td>
    </tr>
</table>

As I said I am a jack of all trades and a master of none.  This makes me a generalist and my greatest asset is that I am quick learner and I learn by doing.

### Working with me

I'm based in the UK and as such my working hours are anywhere between 8am and 7pm.  I have marked time out for Breakfast, Lunch, Dinner, the Gym and Swimming.  Unless I am out I'll normally accept meetings anytime up to 9:30pm UK time Monday - Thursday.  At GitLab we [don't do meetings on Friday](https://handbook.gitlab.com/handbook/communication/#focus-fridays)

The majority of my workflow is done through Slack and my primary channels are: 

<table>
<tr>
<td>
<a href="https://gitlab.slack.com/archives/CN7MPDZF0"><img src="https://img.shields.io/badge/-%23chief--of--staff--team-380D75?&style=plastic&logo=Slack&logoColor=white" /></a>
</td>
<td>
<a href="https://gitlab.slack.com/archives/CKGUGVDNX"><img src="https://img.shields.io/badge/-%23handbook--updates-380D75?&style=plastic&logo=Slack&logoColor=white" /></a>
</td>
<td>
<a href="https://gitlab.slack.com/archives/C81PT2ALD"><img src="https://img.shields.io/badge/-%23handbook-380D75?&style=plastic&logo=Slack&logoColor=white" /></a>
</td>
<td>
<a href="https://gitlab.slack.com/archives/C02GABPC4UV"><img src="https://img.shields.io/badge/-%23internal--handbook-380D75?&style=plastic&logo=Slack&logoColor=white" /></a>
</td>
</tr>
</table>

Supporting [my teams KPIs](https://handbook.gitlab.com/handbook/ceo/chief-of-staff-team/performance-indicators/#percent-of-sent-slack-messages-that-are-not-dms) I prefer to be mentioned in one of the above channels rather than messaged.  This plays in to [GitLabs Values](https://handbook.gitlab.com/values) around [transparancy](https://handbook.gitlab.com/values/#transparency) and [Collaboration](https://handbook.gitlab.com/values/#collaboration).

If you're outside the company then I recommend raising an issue if its about one of the projects to which I am DRI and either assigning the task to me or mentioning me.  If not send an e-mail.

I enjoy pairing when the opportunity presents.  I don't mind the odd meeting to get things resolved quickly

I am not affraid to say I don't know.  However if I don't know do expect me to go off and try to work out the answer.  I am a little chaotic and a little loud.  If I am to loud just let me know.

### My Setups

I have 2 work machines (more by luck than anything else).  They are:

* 13" M1 MacBook Pro (M1, 16GB Ram, 256GB Hard Disk)
* 16" M1 MacBook Pro (M1 Max, 32GB RAM, 512GB Hard Disk)

"2 Laptops!?  Why?!" - I hear you cry.  Well it just happened for a start but actually I have found proper utility in having 2.

I use the 13" Pro for travelling, working away from home and often carrying around the house.  I love the protability of this little machine.  Its not a powerhouse but it does a good job when I am on the go.  I also carry with me a [YubiKey 5c NFC FIP](https://www.yubico.com/gb/product/yubikey-5c-nfc-fips/), a USB-C Multiport Hub, Magic Mouse and a 60W MacBook Pro Charger, in a very small bag.  This is also often carried with my person laptop(s).

Now for my main machine... For that I use the 16" Pro for my Desk as my main work machine hooked up to 3 32" 4K Monitors on a sit/stand desk. The rest of my desk setup is as follow: 

* [Keychrone Q1](https://www.keychron.uk/products/keychron-q1-qmk-custom-mechanical-keyboard-iso-layout-collection) 
* [Logitech MX Master 3S](https://www.logitech.com/en-gb/products/mice/mx-master-3s-mac-bluetooth-mouse.910-006572.html). 
* [Magic Trackpad](https://www.apple.com/uk/shop/product/MK2D3Z/A/magic-trackpad-white-multi-touch-surface)
* [External TouchID Button](https://hackaday.com/2022/12/26/standalone-touch-id-for-your-desktop-mac/)
* [StreamDeck V1](https://www.amazon.co.uk/Elgato-10GAA9901-Stream-Deck/dp/B06XKNZT1P/ref=asc_df_B06XKNZT1P)
* [USB 2.0 extension cradle](https://www.amazon.co.uk/gp/product/B01EI4X0A0/ref=ppx_yo_dt_b_asin_title_o04_s00?ie=UTF8)
* [YubiKey 5 NFC FIPS](https://www.yubico.com/gb/product/yubikey-5-nfc-fips/).
* [Razer Kiyo Pro](https://www.razer.com/gb-en/streaming-cameras/razer-kiyo-pro)
* [Razer Nommo Chroma Speakers](https://www.amazon.co.uk/Razer-Nommo-Chroma-Rear-Facing-Automatic/dp/B078XY4LTV)
* [Razer Firefly V2 Mousemat](https://www.razer.com/gb-en/gaming-mouse-mats/razer-firefly-v2)
* [Razer Kraken Kitty Ears Edition Headset](https://www.amazon.co.uk/Razer-Kraken-Kitty-Gaming-Headset-Classic-Black/dp/B07XYPXB9P/ref=asc_df_B07XYPXB9P)

### My Software Choices

I run the same software setup on both machines using my personal DotFiles repo to share as much configuration as I can.

My current IDE of Choice is [Nova by Panic](https://nova.app/) or [NeoVim](https://neovim.io/) (with [AstroVim](https://astronvim.com/) plugin collection).  My other regular software is:

* Google Chrome (I've tried both Arc and Safari but still end up at Chrome)
* Parallels Desktop running Ubuntu.
* Docker Desktop
* iTerm 2
* Adobe Creative Cloud
* JetBrains (IntelliJ, RubyMine, GoLand, etc)
* VS Code

## Me away from GitLab

Away from GitLab I am a gamer.  I do Console and PC Gaming as well as run a D&D campaighn for 10 players.  I don't drink but I like nothing more than spending an evening in the pub talking and socialising with friends.

I enjoy swimming and Gym but equally I also have a love of Cake.  More an addiction to Cake.  I have been clean of Cake for 2 days at the time of writing.... oh look... Cake!

I am one of those punk/metal-head types who loves wearing Kilts.  So normally when I am out you'll find me wearing a kilt, sporan and big boots.

I am a big Anime and Manga fan.  Some of my favourite Anime/Mangas are:

* Fullmetal Alchimest
* Fairytail
* Seven Deadly Sins
* The Devil is a Part timeer
* Splatoon
* Demon Slayer

and many many more.

### My Home Setup and Gaming

My personal machines are:

* M1 Max MacBook Pro 16" (Same as my GitLab Machine)
* Razer Blade 16 (4090 GPU, dual booting Windows and Linux) - New and Shiny
* Custom gaming rig (11th Gen Intel Core-i9 CPU, Nvidia 3080Ti GPU, 64GB Ram, 2x 2TB SSDs, Dualbooting Windows and Linux) with all the RGB in an Asus ROG Strix Helios case.  

My consoles are:

* Nintendo Switch OLED
* SteamDeck (512GB Model)
* XBox Series X (unloved and seldom used)

Top PC games I can normally be found playing include World of Warcraft, Minecraft, Diablo 4 and Diablo 3.  On the console my XBox is mostly unloved favouring Switch Games like Cult of the Lamb, Splatoon, Zelda and Pokemon.

The kit that makes up the rest of My Home Rig is:

* Pair of Asus ROG 27" 144hz 4k Monitors
* Razer BlackWidow V3 Pro
* Razer Nago Pro Mouse
* Razer FireFly V2
* Razer Nommo Pro Speakers
* Razer Kraken Kitty Ears Headset
* Razer Thunderbolt 4 Dock
* Razer Kiyo Pro Web Cam

### Rig on the Go

As mentioned when I am on the go I have my 13" GitLab MacBook Pro and I'll choose between either my own MacBook Pro or my Razer Blade (favour the blade at the moment).  I also carry a 15.6" external 4k monitor and a Razer Naga mouse.

## Personality Type

My [16 Personality test result](https://www.16personalities.com/free-personality-test/dc173703acad4) are as follows:

* **Personality type:** Entertainer (ESFP-T)
* **Traits:**
    * *Extraverted* – 70%
    * *Observant* – 52%%
    * *Feeling* – 53%
    * *Prospecting* – 76%
    * *Turbulent* – 54%
* **Role:** Explorer
* **Strategy:** Social Engagement

I've tested before as an Adventurer (ISFP-A) before but this was the most recent test result.  One way or another I'm always an explorer.

## GitLab Stats

![GitLab Stats](https://gitlab-readme-stats.vercel.app/api?username=jamiemaynard&show_icons=true&theme=dark)

